import pandas as pd
import sqlite3
import numpy as np


def import_data(db_conn, file_name = 'movie_metadata.csv'):

    df = pd.read_csv(file_name)
    assert df.shape[0] > 0, 'The csv has 0 rows'

    # creating a few normalized tables to make analysis simple

    movie_df = df[['movie_title', 'color', 'director_name', 'num_critic_for_reviews', 'duration',
                   'director_facebook_likes', 'gross', 'num_voted_users', 'cast_total_facebook_likes',
                   'facenumber_in_poster', 'movie_imdb_link', 'num_user_for_reviews', 'language', 'country',
                   'content_rating', 'budget', 'title_year',
                   'imdb_score', 'aspect_ratio', 'movie_facebook_likes']]

    # making the assumption that actor order doesn't matter
    movie_actor_df = pd.melt(df[['movie_title', 'actor_1_name', 'actor_2_name', 'actor_3_name']],
                             'movie_title',
                             var_name='actor_number',
                             value_name='actor')

    # Actors and facebook likes are duplicated in different movies with different values for likes
    # This tells me that likes were captured at different times
    # My assumption about this data is that it was all collected at the same time so
    # some variation in likes may be a data integrity problem.
    # For the purpose of this analysis I will assume that keeping the max value for each actor
    # is a decent approximation of their Facebook popularity

    a1 = df[['actor_1_name', 'actor_1_facebook_likes']]
    a2 = df[['actor_2_name', 'actor_2_facebook_likes']]
    a3 = df[['actor_3_name', 'actor_3_facebook_likes']]
    a1.columns = ['actor', 'likes']
    a2.columns = ['actor', 'likes']
    a3.columns = ['actor', 'likes']

    actor_likes_df = pd.concat([a1, a2, a3], ignore_index=True).drop_duplicates()

    idx = actor_likes_df.groupby(['actor'])['likes'].transform(max) == actor_likes_df['likes']
    actor_likes_df = actor_likes_df[idx]

    assert actor_likes_df['actor'].duplicated().any() == False, 'Expecting unique actor, like combinations'

    # making the assumption that genre number order doesn't matter
    genre = df.genres.str.split('|', expand=True).values.ravel()
    movie_title = np.repeat(df.movie_title.values, len(genre) / len(df))

    movie_genre_df = pd.DataFrame(
        {
            'movie_title': movie_title,
            'genre': genre
        }
    )

    movie_genre_df = movie_genre_df.dropna()

    # making the assumption that genre number order doesn't matter
    plot_keyword = df.plot_keywords.str.split('|', expand=True).values.ravel()
    movie_title = np.repeat(df.movie_title.values, len(plot_keyword) / len(df))

    movie_plot_keyword_df = pd.DataFrame(
        {
            'movie_title': movie_title,
            'plot_keyword': plot_keyword
        }
    )

    movie_plot_keyword_df = movie_plot_keyword_df.dropna()

    movie_df.to_sql('MOVIE', db_conn, if_exists='replace', index=False)
    movie_actor_df.to_sql('MOVIE_ACTOR', db_conn, if_exists='replace', index=False)
    actor_likes_df.to_sql('ACTOR_LIKES', db_conn, if_exists='replace', index=False)
    movie_genre_df.to_sql('MOVIE_GENRE', db_conn, if_exists='replace', index=False)
    movie_plot_keyword_df.to_sql('MOVIE_PLOT_KEYWORD', db_conn, if_exists='replace', index=False)


def top_genres(conn):
    top_genres_df = pd.read_sql_query('''
    with
        profit as (
            select *,
                   gross - budget as profit
            from
                MOVIE)
    select
        genre,
        avg(profit) as avg_profit
    from
        profit as p
        left join
            MOVIE_GENRE as m
            on m.movie_title = p.movie_title
    group by
        genre
    order by
        avg(profit) desc
    limit 10
    ''',
                      conn)
    assert top_genres_df.shape[0] == 10, 'Expecting 10 rows from top_genre'
    return top_genres_df


def top_actors(conn):
    top_actors_df = pd.read_sql_query('''
    with
        profit as (
            select *,
                   gross - budget as profit
            from
                MOVIE)
    select
        actor,
        avg(profit) as avg_profit
    from
        profit as p
        left join
            MOVIE_ACTOR as m
            on m.movie_title = p.movie_title
    group by
        actor
    order by
        avg(profit) desc
    limit 10
    '''
                                   ,conn)

    assert top_actors_df.shape[0] == 10, 'Expecting 10 rows from top_actors'
    return top_actors_df


def top_actor_director(conn):
    top_actor_director_df = pd.read_sql_query('''
    select
        actor||' | '||director_name as actor_director,
        avg(imdb_score) as avg_imbd_score,
        count(distinct p.movie_title) as movies_together
    from
        MOVIE as p
        left join
            MOVIE_ACTOR as m
            on m.movie_title = p.movie_title
    group by
        1
    order by
        avg(imdb_score) desc
    limit 10
    '''
              ,conn)
    assert top_actor_director_df.shape[0] == 10, 'Expecting 10 rows from top_actor_director'

    return top_actor_director_df


if __name__ == '__main__':
    conn = sqlite3.connect('MovieDB.db')

    import_data(conn)
    print("Top 10 movie genres by average profit")
    print(top_genres(conn))
    print("Top 10 actors by average profit")
    print(top_actors(conn))
    print("Top 10 'actors | director' pairs by average IMDB rating")
    print(top_actor_director(conn))
