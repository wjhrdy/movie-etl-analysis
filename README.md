# Movie Test

To run analysis with included text file install `python 3.7`
After you have python 3.7

1. run `pip install -r requirements.txt` in this directory.
2. run `python movie.py`

expected output:
A normalized sqlite database `MovieDB.db`
and text output:
```
Top 10 movie genres by average profit
         genre    avg_profit
0       Family  2.183866e+07
1      Fantasy  1.794854e+07
2        Music  1.697852e+07
3      Musical  1.331062e+07
4        Sport  1.275414e+07
5      Mystery  1.026927e+07
6       Comedy  8.515982e+06
7    Biography  8.110417e+06
8  Documentary  6.838890e+06
9    Animation  2.150895e+06
Top 10 actors by average profit
              actor   avg_profit
0     Gloria Stuart  458672302.0
1     Peter Cushing  449935665.0
2     Niketa Calame  377783777.0
3  Anthony Reynolds  329999255.0
4    Stefan Kapicic  305024263.0
5          Bob Peck  293784000.0
6    Keir O'Donnell  291323553.0
7     Conrad Vernon  286471036.0
8      Sam Anderson  274691196.0
9   Anthony Daniels  272158751.0
Top 10 'actors | director' pairs by average IMDB rating
                          actor_director  avg_imbd_score  movies_together
0         Andrea Martin | John Blanchard             9.5                1
1          Joe Flaherty | John Blanchard             9.5                1
2          Martin Short | John Blanchard             9.5                1
3            Bob Gunton | Frank Darabont             9.3                1
4        Morgan Freeman | Frank Darabont             9.3                1
5          Matthew Ziff | John Stockwell             9.1                1
6            Sam Medina | John Stockwell             9.1                1
7            T.J. Storm | John Stockwell             9.1                1
8       Heath Ledger | Christopher Nolan             9.0                1
9  Robert De Niro | Francis Ford Coppola             9.0                1
```
